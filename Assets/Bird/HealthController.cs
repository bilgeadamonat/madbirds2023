﻿using System;
using SceneSwitcher;
using UnityEngine;

namespace Bird
{
    public class HealthController : MonoBehaviour
    {
        [field: SerializeField] private GameObject CloudParticle { get; set; }
        public static event Action<GameObject> OnTargetDestroyed; 
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                DestroyTarget();
                return;
            }

            if (collision.gameObject.tag == "Crate")
            {
                if (collision.contacts[0].normal.y < -0.5f)
                {
                    DestroyTarget();
                }
            }
        }

        private void DestroyTarget()
        {
            if (OnTargetDestroyed != null)
            {
                OnTargetDestroyed(gameObject);
            }

            Instantiate(CloudParticle, transform.position, new Quaternion(-90f,0f,0f, 0));
            CloudParticle.gameObject.SetActive(true); //Actually not needed.
            CloudParticle.GetComponent<ParticleSystem>().Play();
            
            Destroy(gameObject);
        }
    }
}