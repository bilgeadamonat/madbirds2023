using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bird
{
    public class SceneRestartChecker : MonoBehaviour
    {
        [field:SerializeField] private Rigidbody2D Rigidbody2D { get; set; }
        [field:SerializeField] private MovementController MovementController { get; set; }
        [field:SerializeField, ReadOnly] private float SlowBirdTimeCounter { get; set; }
        [field:SerializeField] private float TimeLimit { get; set; }

        [Header("Screen Borders")]
        [Tooltip("left-bottom and top-right point coordinates of screen.")]
        [SerializeField, ReadOnly] private Vector3 screenBordersMaximum; //TODO: Make this really ReadOnly.
        [SerializeField, ReadOnly] private Vector3 screenBordersMinimum;

        [field: SerializeField, ReadOnly] private bool IsPlayerFrozen { get; set; }
        
        private void Start()
        {
            CacheComponents();
            ResetFields();
            CalculateScreenBorders();
        }

        private void Update()
        {
            RestartGameIfNeeded();
            CountSlowBirdTime();
        }
        
        private void CalculateScreenBorders()
        {
            screenBordersMaximum = Camera.main.ScreenToWorldPoint(
                new Vector2(Screen.width, Screen.height));
            
            screenBordersMinimum = Camera.main.ScreenToWorldPoint(
                new Vector2(0, 0));
            
            Debug.Log("screen borders" + screenBordersMaximum.x + " y: " + screenBordersMaximum.y);
            Debug.Log("screen borders" + screenBordersMinimum.x + " y: " + screenBordersMinimum.y);
        }

        private void CountSlowBirdTime()
        {
            if (MovementController.IsBirdLaunched && Rigidbody2D.velocity.magnitude < 0.2f)
            {
                SlowBirdTimeCounter += Time.deltaTime;
            }
            if (SlowBirdTimeCounter > TimeLimit)
            {
                IsPlayerFrozen = true;
            }
        }

        private void RestartGameIfNeeded()
        {
            if (gameObject.transform.position.y < -3.2f 
                || gameObject.transform.position.y > screenBordersMaximum.y
                || gameObject.transform.position.x > screenBordersMaximum.x
                || gameObject.transform.position.x < screenBordersMinimum.x
                || (MovementController.IsBirdLaunched && IsPlayerFrozen)
               )
            {
                Debug.Log("Scene Will be reloaded.");
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }        
        }

        private void CacheComponents()
        {
            Rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
            MovementController = gameObject.GetComponent<MovementController>();
        }
        
        private void ResetFields()
        {
            SlowBirdTimeCounter = 0;
            IsPlayerFrozen = false;
        }
    }
}
