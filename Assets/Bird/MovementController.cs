﻿using UnityEngine;

namespace Bird
{
    public class MovementController : MonoBehaviour
    {
        #region Variables
        
        private const float MAXIMUM_DRAG_DISTANCE = 2f;
        private SpriteRenderer BirdSpriteRenderer { get; set; }
        [field:SerializeField] private Vector3 Offset { get; set; } //Or simply set Z to 0. Or add the negative of Camera's z.
        [field:SerializeField] public Vector3 InitialPosition { get; set; }
        [field:SerializeField] private float Multiplier { get; set; }
        private Rigidbody2D Rigidbody2D { get; set; }
        [field:SerializeField] private Texture2D CursorOpen { get; set; }
        [field:SerializeField] private Vector2 CursorOpenHotspot { get; set; }
        [field:SerializeField] private Texture2D CursorPointFinger { get; set; }
        [field:SerializeField] private Vector2 CursorPointFingerHotspot { get; set; }
        [field:SerializeField] public bool IsBirdLaunched { get; private set; }

        #endregion

        #region LifecyleMethods

        private void Awake()
        {
            InitialPosition = transform.position;
        }

        private void Start()
        {
            BirdSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
            Rigidbody2D = GetComponent<Rigidbody2D>();
            IsBirdLaunched = false;

            CursorOpenHotspot = new Vector2 (CursorOpen.width / 2, CursorOpen.height / 2);
        }

        private void OnMouseDown()
        {
            if (IsBirdLaunched)
            {
                return; 
            }
            BirdSpriteRenderer.color = Color.red;
            //Cursor.SetCustomCursor();
            Cursor.SetCursor(CursorPointFinger, CursorPointFingerHotspot, CursorMode.Auto);
        }

        private void OnMouseDrag()
        {
            if (IsBirdLaunched)
            {
                return; 
            }

            SetBirdPosition();
        }

        private void OnMouseUp()
        {
            if (IsBirdLaunched)
            {
                return; 
            }
            
            BirdSpriteRenderer.color = Color.white;
            Throw();
            Rigidbody2D.gravityScale = 1;
            
            Cursor.SetCursor(CursorOpen, CursorOpenHotspot, CursorMode.Auto);
        }

        #endregion

        #region ImplementationDetail

        private void SetBirdPosition()
        {
            Vector3 worldPointPositionWithCamerasZ = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 worldPointPosition = worldPointPositionWithCamerasZ + Offset;

            if ((InitialPosition - worldPointPosition).magnitude < MAXIMUM_DRAG_DISTANCE)
            {
                transform.position = worldPointPosition;
            }
            else
            {
                Vector2 calculatedBirdPlace = InitialPosition +
                                              (worldPointPosition - InitialPosition).normalized * MAXIMUM_DRAG_DISTANCE;

                transform.position = calculatedBirdPlace;
            }
        }

        private void Throw()
        {
            Vector2 dragVector = InitialPosition - transform.position;
            Rigidbody2D.AddForce(dragVector * Multiplier);
            IsBirdLaunched = true;
        }

        #endregion
        
    }
}