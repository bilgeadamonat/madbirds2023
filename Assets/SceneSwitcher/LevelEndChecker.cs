﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bird;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneSwitcher
{
    public class LevelEndChecker : MonoBehaviour
    {
        [field: SerializeField] private List<GameObject> TargetList { get; set; }

        private void OnEnable()
        {
            HealthController.OnTargetDestroyed += HandleTargetDestroyed;
        }

        private void OnDisable()
        {
            HealthController.OnTargetDestroyed -= HandleTargetDestroyed;
        }

        private void Start()
        {
            TargetList = GameObject.FindGameObjectsWithTag("Target").ToList(); // This method is quite non-performant.
        }
        
        private void HandleTargetDestroyed(GameObject obj)
        {
            TargetList.Remove(obj);
            if (TargetList.Count == 0)
            {
                Debug.Log("MOVE TO NEXT SCENE!");

                if (SceneManager.GetActiveScene().name == "FirstScene")
                {
                    SceneManager.LoadScene("SecondScene");
                }
            }
        }
        
    }
}