﻿using System;
using Bird;
using UnityEngine;

namespace Trajectory
{
    public class TrajectoryLineController : MonoBehaviour
    {
        private LineRenderer _trajectoryLine;
        private MovementController _movementController;
        
        private void Awake()
        {
            _trajectoryLine = GetComponent<LineRenderer>();
            _movementController = GetComponent<MovementController>();
        }

        private void Start()
        {
            _trajectoryLine.SetPosition(1, _movementController.InitialPosition);
        }

        private void Update()
        {
            //It is better to send an event after launch from MovementController and listen to it here rather than checking each frame.
            if (_movementController.IsBirdLaunched)
            {
                _trajectoryLine.enabled = false;
            }
            else
            {
                _trajectoryLine.enabled = true;
            }
            _trajectoryLine.SetPosition(0, transform.position);
        }
    }
}